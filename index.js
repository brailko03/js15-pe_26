"use strict";
// Теоритичні питання:
// 1. В чому відмінність між setInterval та setTimeout?
// setTimeout дозволяє нам запускати функцію один раз через певний інтервал часу.
// setInterval  запускає функцію не один раз, а регулярно через заданий проміжок часу.

// 2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
// Викликати clearTimeout/clearInterval зі значенням, яке повертає setTimeout/setInterval
// Практичне завдання 1: 

// -Створіть HTML-файл із кнопкою та елементом div.
// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати,
//  що операція виконана успішно.

const buttonElement = document.querySelector(".btn");
const pElement = document.querySelector(".paragraph");

buttonElement.addEventListener("click", (event) => {
    setTimeout(() => {
        pElement.innerText = "Операція виконана успішно!"
    }, 3000);
})

// Практичне завдання 2:

// Реалізуйте таймер зворотного відліку, використовуючи setInterval. 
// При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div.
// Після досягнення 1 змініть текст на "Зворотній відлік завершено".

const timer = document.querySelector(".timer");

let counter = 10;

const timerElement = setInterval(() => {
    timer.textContent = counter;
    counter--;

    if (counter < 0) {
        timer.innerText = "Зворотній відлік завершено"
        clearInterval(timerElement);
    }

}, 1000)
